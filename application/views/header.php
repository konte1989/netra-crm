<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$tplvars = array();

$template = 'header';

$username = $_SESSION['username'];

$userDetails = R::getRow('SELECT * FROM users WHERE username=:username', array('username' => $username));

$tplvars['userDetails'] = $userDetails;

// TUKA KOD

if (isset($_GET['logout'])){
    session_destroy();
    header('Location: /');
    $_SESSION['login'] = false;
}

$tplvars['baseurl'] = base_url();

echo T::mustache($template, $tplvars);