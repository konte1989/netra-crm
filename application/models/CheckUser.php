<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CheckUser extends CI_Model
{

    public function checkUserSession($session)
    {
        if (empty($session) && $session != true ) {
            $session = false;
        } else {
            header('Location: /home');
            $session = true;
        }
        return $session;
    }


}